import "../model/KaffeeMaschine.ecore"

 specification KaffeeMaschineSpecification {

	/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain KaffeeMaschine
	controllable {
		Controller
	}
	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	

	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration KaffeeMaschineCollaboration {

		static role Controller controller
		static role Environment environment
		static role Taster_KaffeeKochen taster_kaffeekochen
		static role Waermegeraete waermegeraete
		static role Zermahlungsgeraete zermahlungsgeraete
		static role Mischungsgeraete mischungsgeraete
		static role Sensor_Pulverkaffee sensor_pulverkaffee
		static role Sensor_Kaffee sensor_kaffee
		static role Sensor_Temp sensor_temp
		static role Taster_AuslaufStart taster_auslaufstart
		static role Taster_AuslaufStop taster_auslaufstop
		static role Auslauf auslauf
		/* 
		guarantee scenario WennTaster_KaffeekochenPressedDannZermahlen {
			 taster_kaffeekochen -> controller.taster_kaffeekochenPressed()
			 strict urgent controller -> zermahlungsgeraete.starteMalvorgang()
		}
		
		assumption scenario PulverkaffeeWillBeCreated{
			controller -> zermahlungsgeraete.starteMalvorgang()
			strict eventually zermahlungsgeraete -> controller.pulverkaffeeCreated()
		}
		
		guarantee scenario WennPulverkaffeeCreatedDannMahlenStopundWasserKochen{
			zermahlungsgeraete -> controller.pulverkaffeeCreated()
			strict urgent controller -> zermahlungsgeraete.stoppen()
			strict urgent controller -> waermegeraete.starteKochen()
		}
		
		assumption scenario WennTempBis100GradDannKochStopUndStarteMischen{
			controller -> waermegeraete.starteKochen()
			strict eventually waermegeraete -> controller.kochStoppen()
			strict eventually waermegeraete -> controller.starteMischung()
		}
		*/
		
		guarantee scenario WennTaster_KaffeekochenPressedDannZermahlen {
			 taster_kaffeekochen -> controller.taster_kaffeekochenPressed()
			 strict urgent controller -> zermahlungsgeraete.setZermahlen(true)
		}
		
		assumption scenario PulverkaffeeWillBeCreated{
			controller -> zermahlungsgeraete.setZermahlen(true)
			strict eventually zermahlungsgeraete -> controller.pulverkaffeeCreated()
			
		}
	  
	 
		assumption scenario WennZermahlenDannTaster_KaffeekochenKeinReaktion{
			taster_kaffeekochen -> controller.taster_kaffeekochenPressed()
			violation  [zermahlungsgeraete.zermahlen || waermegeraete.kochen || mischungsgeraete.mischung || waermegeraete.warmHalten ||auslauf.opened ]
		}
		
		 
		guarantee scenario WennPulverkaffeeCreatedDannZermahlhungStopUndWasserKochen{
			zermahlungsgeraete -> controller.pulverkaffeeCreated()
			interrupt[!zermahlungsgeraete.zermahlen]
			strict urgent controller -> zermahlungsgeraete.setZermahlen(false)
			strict urgent controller -> waermegeraete.setKochen(true)
		}
		
		 
		assumption scenario WennTempBis100GradDannKochStoppen{
			controller -> waermegeraete.setKochen(true)
			interrupt[!waermegeraete.kochen]
			strict eventually waermegeraete -> controller.kochStoppen()
		}
		
		guarantee scenario WennKochStoppenDannStarteMischung{
			waermegeraete -> controller.kochStoppen()
			interrupt[!waermegeraete.kochen]
			strict urgent controller -> waermegeraete.setKochen(false)
			strict urgent controller -> mischungsgeraete.setMischung(true)
		}
		
		
		assumption scenario NachDreiSekundeDannWarmHalten{
			controller -> mischungsgeraete.setMischung(true)
			interrupt[!mischungsgeraete.mischung]
			strict eventually mischungsgeraete -> controller.endMischung()
			strict eventually mischungsgeraete -> controller.starteWarmhalte()
		}
		
		guarantee scenario WennStartWarmhalteDannMischungEndUndStarteWaremgeraete{
			mischungsgeraete -> controller.starteWarmhalte()
			interrupt[!mischungsgeraete.mischung]
			strict urgent controller -> mischungsgeraete.setMischung(false)
			strict urgent controller -> waermegeraete.setWarmHalten(true)
		}
		
		
		guarantee scenario WennTaster_AuslaufstartPressedDannAuslaufen{
			taster_auslaufstart -> controller.taster_auslaufstartpressed()
			interrupt[!waermegeraete.warmHalten ||auslauf.opened ]
			//strict urgent controller -> waermegeraete.setWarmHalten(false) 
			strict urgent controller -> auslauf.setOpened(true)
		}
		
		guarantee scenario WennTaster_AuslaufstopPressedDannAuslaufStop{
			taster_auslaufstop -> controller.taster_auslaufstoppressed()
			interrupt[!auslauf.opened || !waermegeraete.warmHalten]
			strict urgent controller -> auslauf.setOpened(false)
			//strict urgent controller -> auslauf.setClosed(true)
		}
		
		assumption scenario WennAuslaufOpendDannKeinKaffeeMehr{
			controller -> auslauf.setOpened(true)
			interrupt[!auslauf.opened || !waermegeraete.warmHalten]
			//strict urgent controller -> auslauf.setOpened(false)
			//strict urgent controller -> waermegeraete.setWarmHalten(false)
			strict eventually auslauf -> controller.keinKaffeeMehr()
		}
		
		guarantee scenario WennKeinKaffeeMehrDannZuInitState{
			auslauf -> controller.keinKaffeeMehr()
			interrupt[!auslauf.opened || !waermegeraete.warmHalten]
			strict urgent controller ->  auslauf.setOpened(false)
			strict urgent controller -> waermegeraete.setWarmHalten(false)
		}	
	}

}
